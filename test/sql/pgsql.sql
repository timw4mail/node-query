-- sample data to test PostgreSQL INFORMATION_SCHEMA

-- Table create_join
CREATE TABLE create_join (
    id integer NOT NULL,
    key text,
    val text
);

CREATE TABLE create_test (
    id integer NOT NULL,
    key text,
    val text
);